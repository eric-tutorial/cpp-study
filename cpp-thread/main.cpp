#include <thread>
#include <iostream>

using LL = long long;

void calc(LL start, LL end) {
    LL ret = 0;
    for (LL i = start; i <= end; i++) {
        ret += i;
    }
    std::cout << ret << std::endl;
}

//  多线程  g++ -std=c++11 -o main main.cpp
int main() {
    std::thread t = std::thread(calc, 1, 100000);
    std::thread t1 = std::thread(calc, 1, 10);
    t.join();
    t1.join();
}