#include <iostream>
#include "PersonInfo.h"

int main(){

    // 调用静态方法
    PersonInfo::setName("eric");

    std::string name = PersonInfo::getName();
    
    std::cout << "get Value :" << name << std::endl;

    // 实例化与静态方法没关系
    PersonInfo personInfo;

    return 0;
}
