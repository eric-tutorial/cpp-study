#include <iostream>
using namespace std;

#include "Singleton.h"

// 主函数，程序从这里运行，可以尝试debug
int main(){
    // :: 调用静态方法
    // -> 调用成员方法
    Singleton::instance() ->show();
    return 0;
}