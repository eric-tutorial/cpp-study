#include <iostream>

// https://zhuanlan.zhihu.com/p/280765580?utm_id=0

//鸟
class Bird{
public:
    Bird(){}
    virtual ~Bird(){}
    virtual void Fly() {
        std::cout << "I am  a bird and I am flying" << std::endl;
    }
};

//燕子  一个冒号 继承
class Swallow : public Bird{
public:
    Swallow(){}
    ~Swallow(){}
    void Fly() override { // override 关键字重写父类的方法
        std::cout << "I am  a Swallow and I am flying" << std::endl;
    }
};

//大雁
class WildGoose : public Bird{
public:
    WildGoose(){}
    ~WildGoose(){}
    void Fly() override {
        std::cout << "I am  a Wild Goose and I am flying" << std::endl;
    }
};

//模拟鸟的飞行
void Fly(Bird& b){
    b.Fly();
}

int main(int argc,char * argv[]){
    WildGoose goose;
    Swallow s = Swallow();

    Fly(s);
    Fly(goose);


    // 父类的初始化实例化
    Bird bird;
    bird.Fly();
}