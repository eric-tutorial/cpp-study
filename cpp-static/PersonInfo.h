#include<iostream>

class PersonInfo 
{
    public:
    PersonInfo(); // 构造器

    static void setName(std::string name); // 静态变量
    static std::string getName(); // 静态变量

    public:
    static std::string name; // 静态变量
};

// 这个初始化必须放在 *.cpp文件里，因为放.h文件就会赋值多次。
// std::string PersonInfo::name = "init value";