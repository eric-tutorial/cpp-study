```shell
vcpkg install jsoncpp
```

https://blog.csdn.net/weixin_40448140/article/details/109111042

[【腾讯文档】cpp基础练习](https://docs.qq.com/doc/DSGtMTFNnUHBuYXJ1)

# /usr/bin/ld: cannot find -ljsoncpp_object: No such file or directory

```cpp
====================[ Build | cpp_clion | Debug ]===============================
/home/eric/.local/share/JetBrains/Toolbox/apps/clion/bin/cmake/linux/x64/bin/cmake --build /home/eric/Project/Cpp/cpp-study/cpp-clion/cmake-build-debug --target cpp_clion -j 14
[0/1] Re-running CMake...
CMake Deprecation Warning at /home/eric/Project/Cpp/vcpkg/installed/x64-linux/share/jsoncpp/jsoncppConfig.cmake:2 (cmake_policy):
  Compatibility with CMake < 3.5 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.
Call Stack (most recent call first):
  /home/eric/Project/Cpp/vcpkg/scripts/buildsystems/vcpkg.cmake:859 (_find_package)
  CMakeLists.txt:6 (find_package)


-- Configuring done (0.0s)
-- Generating done (0.0s)
-- Build files have been written to: /home/eric/Project/Cpp/cpp-study/cpp-clion/cmake-build-debug
[1/1] Linking CXX executable cpp_clion
FAILED: cpp_clion 
: && /usr/bin/c++ -g  CMakeFiles/cpp_clion.dir/main.cpp.o -o cpp_clion  -ljsoncpp_object  /home/eric/Project/Cpp/vcpkg/installed/x64-linux/debug/lib/libjsoncpp.a && :
/usr/bin/ld: cannot find -ljsoncpp_object: No such file or directory
collect2: error: ld returned 1 exit status
ninja: build stopped: subcommand failed.
```

![Snipaste_2024-05-13_22-03-16.png](Snipaste_2024-05-13_22-03-16.png)

## 这里`target_link_libraries(cpp_clion PRIVATE jsoncpp_object jsoncpp_static)`的 jsoncpp_object 是啥意思

`target_link_libraries` 是 CMake 中用于将目标与特定库进行链接的命令。在你提供的命令中，`cpp_clion`
是你的目标名称，`PRIVATE` 是指定链接的范围（在这种情况下，链接是私有的，只影响当前目标），而 `jsoncpp_object`
和 `jsoncpp_static` 是要链接的库的名称。

在 CMake 中，可以链接不同类型的库，例如静态库（`.a` 文件）和动态库（`.so` 或 `.dll` 文件）。`jsoncpp_object`
和 `jsoncpp_static` 可能是你项目中使用的 JSONCpp 库的两个不同类型的构建目标。通常，`jsoncpp_object`
可能是一个对象库（用于生成静态库或动态库），而 `jsoncpp_static` 则是 JSONCpp 的静态库。

因此，`target_link_libraries(cpp_clion PRIVATE jsoncpp_object jsoncpp_static)` 表示将 `cpp_clion`
目标链接到 `jsoncpp_object` 和 `jsoncpp_static` 这两个库。