#pragma once
#include <iostream>

using namespace std; // 否则NULL无法正常导入。

class Singleton 
{

// 定义静态方法
public:
 static Singleton *instance()
 {
    if (m_instance == NULL){
         m_instance = new Singleton();
    }

    return m_instance;
 }

// 定义成员方法
 void show(){
    std::cout << m_name << std::endl;
 }

// 定义构造器
private:   
    Singleton() : m_name("A") {}
    Singleton(const Singleton &){}
    ~Singleton(){}
    Singleton & operator = (const Singleton &);


//  定义成员变量
private:
    string m_name; // 静态变量
    static Singleton * m_instance;

};

Singleton * Singleton::m_instance=NULL;