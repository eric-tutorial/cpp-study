#include <thread>
#include <iostream>

using namespace std;
using LL = long long;


int main(){

    // 匿名函数
    auto f = [](LL start,LL end){
        LL sum = 0;
        for(LL i = start; i <=end;i++){
            sum += i;
        }
        cout << sum << endl;
    };

    thread t = thread(f,1,100000);
    t.join();

}