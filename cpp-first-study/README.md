Cpp
===

```shell
g++.exe .\demo.cpp .\main.cpp -o demo.exe
```

linux下

```
gcc main.cpp demo.h demo.cpp  -o main
```

![](assets/20221023_221708_image.png)

linux下debug
![](assets/linux-debug.png)

注意配置文件


![](assets/20221128_155415_image.png)

Clion没有跑起来

![img.png](img.png)


推荐*.h和 *.cpp写在一个文件里 *.hpp就好了。