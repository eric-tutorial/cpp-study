#include <iostream>
#include <cpr/cpr.h>

using namespace std;

int main(int argc, char const *argv[])
{
    auto response = cpr::Get(cpr::Url{"http://httpbin.org"});

    cout << response.status_code << endl;

    cout << response.text << endl;

    
    return 0;
}
