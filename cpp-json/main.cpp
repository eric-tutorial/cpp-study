#include <iostream>
#include <json/json.h>

int main() {
    Json::Value root;
    root["one"] = "one";
    root["number"] = 1;
    root["float"] = 1.4;
    std::cout << root.toStyledString() << std::endl;
    return 0;
}
