Cpp
===

windows
```shell
g++.exe .\demo.cpp .\main.cpp -o demo.exe
```

linux
```shell
gcc demo.cpp main.cpp -o demo
```

![](assets/20221023_221708_image.png)


# 自己写的头文件导入

必须使用双引号导入，不能使用 <>

https://blog.csdn.net/qq_43594926/article/details/123824392

# 双重校验锁。

单例模式.