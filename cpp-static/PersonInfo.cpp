#include <iostream>
#include "PersonInfo.h"

PersonInfo::PersonInfo(){
    std::cout<< "实例化"<<std::endl;
}

std::string PersonInfo::getName(){
    // return PersonInfo::name;
    return name;
}

void PersonInfo::setName(std::string name){
    
    PersonInfo::name = name;

    std::cout<< "静态方法赋值"<< PersonInfo::name <<std::endl;
}

// 这个初始化必须放在 *.cpp文件里，因为放.h文件就会赋值多次。
std::string PersonInfo::name = "init value";