#include <stdio.h>
#include <iostream>
#include <string.h>
#include <map> 

using namespace std;

// map 基础学习 https://www.w3cschool.cn/cpp/cpp-fu8l2ppt.html

int main(){
    
    printf("map stduy\n");
    
    std::map<int , std::string> mapPerson;
    
    mapPerson.insert(std::map < int, std::string > ::value_type (2, "Tom"));
    
    mapPerson[3] = "Jerry";



    std::map < int ,std::string > ::iterator it;
        std::map < int ,std::string > ::iterator itEnd;
        it = mapPerson.begin();
        itEnd = mapPerson.end();
        while (it != itEnd) {
        cout<<it->first<<' '<<it->second<<endl;  
        it++;
    }

    return 0;
}