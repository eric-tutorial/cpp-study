cpp json
===

# 配置库

https://blog.csdn.net/weixin_40448140/article/details/109111042

打开 File -> Settings -> Build, Execution, Deployment -> CMake

```shell
# linux
-DCMAKE_TOOLCHAIN_FILE=/home/eric/Project/Cpp/git/vcpkg/scripts/buildsystems/vcpkg.cmake

# macbook
-DCMAKE_TOOLCHAIN_FILE=/Users/HOX4SGH/Project/CPP/vcpkg/scripts/buildsystems/vcpkg.cmake
```

![img.png](assets/img.png)

安装json处理依赖

```shell
vcpkg install jsoncpp
```

# 精度问题

![img_1.png](assets/img_1.png)

# vcpkg安装

~~目前不支持macbook arm系列~~ , 2024年可以用了。

https://github.com/Microsoft/vcpkg

安装指导

https://github.com/microsoft/vcpkg/blob/master/README_zh_CN.md

#  

```cpp
====================[ Build | cpp_json | Debug ]================================
/Applications/CLion.app/Contents/bin/cmake/mac/aarch64/bin/cmake --build /Users/HOX4SGH/Project/CPP/cpp-study/cpp-json/cmake-build-debug --target cpp_json -j 8
[1/1] Linking CXX executable cpp_json
FAILED: cpp_json 
: && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++ -g -arch arm64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX14.2.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names  CMakeFiles/cpp_json.dir/main.cpp.o -o cpp_json  -ljsoncpp_object  /Users/HOX4SGH/Project/CPP/vcpkg/installed/arm64-osx/debug/lib/libjsoncpp.a && :
ld: library 'jsoncpp_object' not found
clang: error: linker command failed with exit code 1 (use -v to see invocation)
ninja: build stopped: subcommand failed.
```

将[CMakeLists.txt](CMakeLists.txt) 里的jsoncpp_object删除。

```
target_link_libraries(cpp_json PRIVATE jsoncpp_object jsoncpp_static)

改成

target_link_libraries(cpp_json PRIVATE jsoncpp_static)
```