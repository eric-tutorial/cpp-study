#include "stdio.h"

#include "singleton.h" //  注意这里不能使用 <> 必须是双引号。

int main(int argc, char const *argv[])
{

    /* code */

   SingleInstance* singleInstance = SingleInstance::GetInstance();

   singleInstance->Print();

   SingleInstance* singleInstance2 = SingleInstance::GetInstance();
   // 每次一样
   singleInstance2->Print();

    return 0;
}
