# 命令行直接运行

```shell
g++ main.cpp PersonInfo.cpp  && ./a.out 
```

# macbook下直接编译运行

![](assets/20221211_185249_image.png)


# clang和g++是不同的编译器实现

当代码有std::cout的时候，下面命令并不能编译通过。但是g++可以的。

```xshell
clang main.cpp PersonInfo.cpp
```

