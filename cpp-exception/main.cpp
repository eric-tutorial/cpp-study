#include <iostream>

using namespace std;

// 引入一个中间层来获得更高的可读性、安全性和灵活性。
// [[noreturn]]
void raise(const char *msg) {
    std::cout << "下面抛出异常" << endl;
    throw std::runtime_error(msg);
}

void try_catch_function()
try {
    raise("try_catch_function 的异常，把异常和函数直接结合了，免去了方法括号");
}
catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
}

//  g++  main.cpp  && ./a.out
int main() {

    try {
        throw 1;
    }
    catch (int e) {
        cerr << e << endl;
    }

    try {
        throw std::runtime_error("1");
    }
    catch (...) {
        cerr << "抛出了异常" << endl;
    }

    try {
        raise("error occured");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
    }

    try_catch_function();

    return 0;
}